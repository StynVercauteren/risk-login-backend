package be.kdg.riskymerge.login.Unit;

import antlr.TokenStreamException;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.TokenRepository;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.Auth.ConfirmPasswordTokenService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConfirmPasswordTest {
    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfirmPasswordTokenService confirmPasswordTokenService;

    private static Token testToken;

    @BeforeAll
    void setup() {
        testToken = new Token();
        testToken.setToken("a0c90614-ef9a-4fdc-96f2-0371cdde2354");
        User linkedUser = userRepository.findById(1);
        testToken.setUser(linkedUser);
        tokenRepository.save(testToken);
    }

    @Test
    public void testConfirmToken() throws TokenStreamException {
        Boolean result = confirmPasswordTokenService.validatePasswordResetToken(1, testToken.getToken());
        Assert.assertEquals(true, result);
    }

    @Test
    public void testTokenNotFound() {
        assertThrows(TokenStreamException.class, () -> {
            confirmPasswordTokenService.validatePasswordResetToken(1, "UnknownToken");
        });
    }

    @Test
    public void testUsernameNotFound() {
        assertThrows(UsernameNotFoundException.class, () -> {
            confirmPasswordTokenService.validatePasswordResetToken(2, testToken.getToken());
        });
    }
}

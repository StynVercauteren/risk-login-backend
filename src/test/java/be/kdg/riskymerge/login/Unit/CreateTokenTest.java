package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.TokenRepository;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.CreateTokenService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateTokenTest {
    private static User testUser;
    private static String testToken;

    @Autowired
    private CreateTokenService createTokenService;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeAll
    void setup() {
        testUser = userRepository.findByUsername("Styn");
        testToken = "a0c90614-ef9a-4fdc-96f2-0371cdde1574";
    }

    @Test
    public void createPasswordTokenTest() {
        createTokenService.createVerificationToken(testUser, testToken);
        Token token = tokenRepository.findByToken(testToken);
        Assert.assertNotNull("Token should not be null", token);
    }
}

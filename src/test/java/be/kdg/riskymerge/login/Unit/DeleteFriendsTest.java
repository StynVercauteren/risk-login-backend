package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.Friends.DeleteFriendService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeleteFriendsTest {
    @Autowired
    private DeleteFriendService deleteFriendService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    @Autowired
    private UserRepository userRepository;

    private static User testUser;
    private static User tweedeUser;

    @BeforeAll
    void setup() {
        testUser = new User();
        testUser.setUsername("TestUser22");
        testUser.setEmail("TestUser22@outlook.com");
        testUser.setPassword("TestUser22");
        testUser.setEnabled(true);

        tweedeUser = new User();
        tweedeUser.setUsername("tweedeUser");
        tweedeUser.setEmail("tweedeUser@outlook.com");
        tweedeUser.setPassword("tweedeUser");
        tweedeUser.setEnabled(true);

        userRepository.save(testUser);
        userRepository.save(tweedeUser);

        testUser.getFriends().add(tweedeUser);
        tweedeUser.getFriends().add(testUser);

        userRepository.save(testUser);
        userRepository.save(tweedeUser);
    }

    @Test
    public void testDeleteFriend() {
        String result = deleteFriendService.deleteFriend(testUser, tweedeUser);
        Assert.assertEquals("Removed friend!", result);
        Assert.assertTrue("User 1 should have no friends", testUser.getFriends().size() == 0);
        Assert.assertTrue("User 2 should have no friends", tweedeUser.getFriends().size() == 0);
    }
}

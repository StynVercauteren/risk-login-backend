package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.managers.RemoveAccountManager;
import be.kdg.riskymerge.models.DTO.SignupDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.services.Auth.SignupService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.RemoveAccountService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.naming.AuthenticationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RemoveAccountServiceTest {
    @Autowired
    private RemoveAccountService removeAccountService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    private static SignupDTO testUser;

    @Autowired
    private SignupService signupService;

    @BeforeAll
    void setup() throws AuthenticationException {
        testUser = new SignupDTO();
        testUser.setUsername("userToDelete");
        testUser.setEmail("userToDelete@outlook.com");
        testUser.setPassword("userToDelete");

        signupService.register(testUser);
    }

    @Test
    public void testDeleteAccount() throws AuthenticationException {
        JwtResponse newUser = new JwtResponse();
        newUser.setUsername("userToDelete");

        User dbUser = getUserByUsernameService.getUserByUsername(newUser.getUsername());

        String result = removeAccountService.remove(newUser, dbUser);
        Assert.assertEquals("User deleted succesfully", result);

        assertThrows(UsernameNotFoundException.class, () -> {
            getUserByUsernameService.getUserByUsername("userToDelete");
        });
    }
}

package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.DTO.SignupDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.Auth.SignupService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.naming.AuthenticationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class RegisterTest {
    @Autowired
    private SignupService signupService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testRegister() throws AuthenticationException {
        SignupDTO testUser = new SignupDTO();

        testUser.setUsername("testuser");
        testUser.setEmail("testuser@outlook.com");
        testUser.setPassword("testpasswoord");

        String result = signupService.register(testUser);
        Assert.assertEquals("User registered successfully!", result);
        User checkUser = userRepository.findByUsername(testUser.getUsername());
        Assert.assertEquals("testuser", checkUser.getUsername());
        Assert.assertEquals("testuser@outlook.com", checkUser.getEmail());
    }

    @Test
    public void testUsernameAlreadyTaken() {
        SignupDTO testUser = new SignupDTO();
        testUser.setUsername("Styn");

        assertThrows(AuthenticationException.class, () -> {
            signupService.register(testUser);
        });
    }

    @Test
    public void testEmailAlreadyTaken() {
        SignupDTO testUser = new SignupDTO();
        testUser.setEmail("gtamichaelke123@outlook.com");

        assertThrows(AuthenticationException.class, () -> {
            signupService.register(testUser);
        });
    }
}

package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.DTO.UpdateAccountDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.GetUser.GetUserByIdService;
import be.kdg.riskymerge.services.UpdateAccountService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class UpdateUserServiceTest {
    private UpdateAccountDTO testUser;
    private User dbUser;

    @Autowired
    private UpdateAccountService updateAccountService;

    @Autowired
    private GetUserByIdService getUserByIdService;

    @BeforeEach
    void setup() {
        this.testUser = new UpdateAccountDTO();
        this.testUser.setId(1);

        this.dbUser = getUserByIdService.getUserById(testUser.getId());
    }

    @Test
    public void testUpdateUsername() {
        this.testUser.setUsername("UpdatedUser");

        String result = updateAccountService.updateAccount(this.testUser, dbUser);
        Assert.assertEquals("username", result);
        Assert.assertEquals("UpdatedUser", getUserByIdService.getUserById(testUser.getId()).getUsername());
    }

    @Test
    public void testUpdateEmail() {
        this.testUser.setEmail("updatedMail@outlook.com");

        String result = updateAccountService.updateAccount(this.testUser, dbUser);
        Assert.assertEquals("email", result);
        Assert.assertEquals("updatedMail@outlook.com", getUserByIdService.getUserById(testUser.getId()).getEmail());
    }

    @Test
    public void testUpdatePassword() {
        this.testUser.setPassword("NewTestWachtwoord");

        String result = updateAccountService.updateAccount(this.testUser, dbUser);
        Assert.assertEquals("password", result);
    }

}

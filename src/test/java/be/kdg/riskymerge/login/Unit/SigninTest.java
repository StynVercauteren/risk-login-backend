package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.managers.AuthManager;
import be.kdg.riskymerge.models.DTO.LoginDTO;
import be.kdg.riskymerge.models.DTO.UpdateAccountDTO;
import be.kdg.riskymerge.models.DTO.UserDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.services.Auth.SigninService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.naming.AuthenticationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SigninTest {
    @Autowired
    private SigninService signinService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    private LoginDTO testUser;
    private User dbUser;

    @BeforeAll
    void setup() {
        testUser = new LoginDTO();
    }

    @Test
    public void testSignIn() throws AuthenticationException {
        this.testUser.setUsername("Styn");
        this.testUser.setPassword("testwachtwoord");

        dbUser = getUserByUsernameService.getUserByUsername(testUser.getUsername());

        JwtResponse result = signinService.signin(testUser, dbUser);
        Assert.assertEquals("Styn", result.getUsername());
        Assert.assertEquals("saul.jpg", getUserByUsernameService.getUserByUsername(testUser.getUsername()).getProfilepicture());
    }

    @Test
    public void testUserNotEnabled() {
        testUser = new LoginDTO();
        testUser.setUsername("notEnabledTestUser");
        testUser.setPassword("notEnabledTestUser");
        dbUser = getUserByUsernameService.getUserByUsername(testUser.getUsername());

        assertThrows(AuthenticationException.class, () -> {
            signinService.signin(testUser, dbUser);
        });
    }

}

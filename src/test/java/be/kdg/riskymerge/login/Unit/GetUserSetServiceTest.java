package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.Friends.GetUserSetService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetUserSetServiceTest {
    @Autowired
    private GetUserSetService getUserSetService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    private User user;
    private User otherUser;

    @BeforeAll
    void setup() {
        user = getUserByUsernameService.getUserByUsername("Styn");
        otherUser = getUserByUsernameService.getUserByUsername("Stino");
    }
    @Test
    public void testGetFriends() {
        Set<User> result;
        result = getUserSetService.getSet(user, "friend");
        Assert.assertEquals(2, result.size());

        result = getUserSetService.getSet(otherUser, "friend");
        Assert.assertEquals(0, result.size());

        result = getUserSetService.getSet(otherUser, "pending");
        Assert.assertEquals(1, result.size());

        result = getUserSetService.getSet(user, "pending");
        Assert.assertEquals(0, result.size());

        result = getUserSetService.getSet(user, "sent");
        Assert.assertEquals(1, result.size());

        result = getUserSetService.getSet(otherUser, "sent");
        Assert.assertEquals(0, result.size());
    }
}

package be.kdg.riskymerge.login.Unit;

import be.kdg.riskymerge.models.DTO.LoginDTO;
import be.kdg.riskymerge.models.DTO.UserDTO;
import be.kdg.riskymerge.services.Auth.VerifyUserService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.naming.AuthenticationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class VerifyUserTest {
    @BeforeAll
    void setup() {
        userDTOUser = new UserDTO();
        userDTOUser.setToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTdGlubyIsImlhdCI6MTU4MzkyODk1NSwiZXhwIjoxNTg0MDE1MzU1fQ.h6l3TRGrTz_uOi2sz_IT229U88C2gsnsVC-PrZz9--388XR50kl9hpJFC_GanmgNVYwFkXTi9BHu9uGL1sU0HQ");
    }

    private UserDTO userDTOUser;

    @Autowired
    private VerifyUserService verifyUserService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    @Test
    public void testVerifyUser() throws AuthenticationException {
        userDTOUser.setUsername("Stino");

        Boolean result = verifyUserService.verifyUser(userDTOUser, getUserByUsernameService.getUserByUsername(userDTOUser.getUsername()));
        assertEquals(true, result);
    }

    @Test
    public void testWrongToken() {
        userDTOUser.setUsername("Stino");
        userDTOUser.setToken("testToken");

        assertThrows(AuthenticationException.class, () -> {
            verifyUserService.verifyUser(userDTOUser, getUserByUsernameService.getUserByUsername(userDTOUser.getUsername()));
        });
    }

    @Test
    public void testUsernameAlreadyTaken() {
        userDTOUser.setUsername("OnbestaandeUser");

        assertThrows(UsernameNotFoundException.class, () -> {
            verifyUserService.verifyUser(userDTOUser, getUserByUsernameService.getUserByUsername(userDTOUser.getUsername()));
        });
    }
}

package be.kdg.riskymerge.login.Integration;

import antlr.TokenStreamException;
import be.kdg.riskymerge.managers.AuthManager;
import be.kdg.riskymerge.models.DTO.LoginDTO;
import be.kdg.riskymerge.models.DTO.SignupDTO;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.repositories.TokenRepository;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.Auth.SigninService;
import be.kdg.riskymerge.services.Auth.SignupService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.naming.AuthenticationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class NewUserTest {
    @Autowired
    private AuthManager authManager;

    @Autowired
    private SignupService signupService;

    @Autowired
    private SigninService signinService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    private SignupDTO testUser;

    @Test
    public void testNewUser() throws AuthenticationException, TokenStreamException {
        testUser = new SignupDTO();
        testUser.setUsername("userForIntegration");
        testUser.setEmail("userForIntegration@outlook.com");
        testUser.setPassword("userForIntegration");

        String result = signupService.register(testUser);
        Assert.assertEquals("User registered successfully!", result);
        User checkUser = userRepository.findByUsername(testUser.getUsername());
        Assert.assertEquals("userForIntegration", checkUser.getUsername());
        Assert.assertEquals("userforintegration@outlook.com", checkUser.getEmail());
        Assert.assertFalse(checkUser.isEnabled());

        Token verificationToken = tokenRepository.findByUser(checkUser);
        Assert.assertNotNull(verificationToken);

        authManager.confirmEmail(verificationToken.getToken());
        User checkUserNaTokenVerificatie = userRepository.findByUsername(testUser.getUsername());
        Assert.assertTrue(checkUserNaTokenVerificatie.isEnabled());

        LoginDTO loginDTO = new LoginDTO(testUser.getUsername(), testUser.getPassword());
        JwtResponse response = signinService.signin(loginDTO, checkUserNaTokenVerificatie);
        Assert.assertEquals(checkUserNaTokenVerificatie.getUser_id(), response.getId());
        Assert.assertEquals(checkUserNaTokenVerificatie.getUsername(), response.getUsername());
        Assert.assertEquals(checkUserNaTokenVerificatie.getEmail(), response.getEmail());
    }

    @Test
    public void testUserNotEnabled() throws AuthenticationException {
        testUser = new SignupDTO();
        testUser.setUsername("userForNotEnabled");
        testUser.setEmail("userForNotEnabled@outlook.com");
        testUser.setPassword("userForNotEnabled");

        signupService.register(testUser);
        User checkUser = userRepository.findByUsername(testUser.getUsername());
        LoginDTO loginDTO = new LoginDTO(testUser.getUsername(), testUser.getPassword());

        assertThrows(AuthenticationException.class, () -> {
            signinService.signin(loginDTO, checkUser);
        });
    }
}

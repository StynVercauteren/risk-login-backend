package be.kdg.riskymerge.login.Integration.Controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
public class CheckIfUserHasPendingRequestTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkIfUserHasPendingRequestTest() throws Exception {
        mockMvc.perform(get("/api/friends/checkIfUserHasPendingRequest")
                .param("currentUserName","Styn")
                .param("otherUserName","Stino"))
                .andExpect(status().isOk());
    }
}

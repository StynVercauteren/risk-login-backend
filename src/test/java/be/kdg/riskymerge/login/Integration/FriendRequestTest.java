package be.kdg.riskymerge.login.Integration;

import be.kdg.riskymerge.managers.AuthManager;
import be.kdg.riskymerge.managers.FriendRequestManager;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.FriendRequest.SendFriendRequestService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.SendMailService;
import be.kdg.riskymerge.services.SendUserNotificationService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FriendRequestTest {
    @Autowired
    private FriendRequestManager friendRequestManager;

    @Autowired
    private SendFriendRequestService sendFriendRequestService;

    @Autowired
    private GetUserByUsernameService getUserByUsernameService;

    @Mock
    private SendUserNotificationService sendUserNotificationService;

    @BeforeAll
    void setup() {
        ReflectionTestUtils.setField(friendRequestManager, "sendUserNotificationService" , sendUserNotificationService);
    }

    @Test
    public void testAcceptRequest() {
        User jens = getUserByUsernameService.getUserByUsername("Jens");
        User AI = getUserByUsernameService.getUserByUsername("AIAccount");

        String result = sendFriendRequestService.sendFriendRequest(jens, AI);
        Assert.assertEquals("Sent pending friend request!", result);

        User jensNaSend = getUserByUsernameService.getUserByUsername("Jens");
        User AINaSend = getUserByUsernameService.getUserByUsername("AIAccount");

        Assert.assertEquals("User Jens should have 1 sent request", jensNaSend.getSentRequests().size(), 1);
        Assert.assertEquals("User AIAccount should have 1 pending request", AINaSend.getPendingRequests().size(), 1);

        String reply = friendRequestManager.acceptFriendRequest(jensNaSend.getUsername(), AINaSend.getUsername());
        Assert.assertEquals("You are now friends with " + AINaSend.getUsername(), reply);

        User jensNaAccept = getUserByUsernameService.getUserByUsername("Jens");
        User AINaAccept = getUserByUsernameService.getUserByUsername("AIAccount");

        Assert.assertEquals("User Jens should have 0 sent requests", jensNaAccept.getSentRequests().size(), 0);
        Assert.assertEquals("User AIAccount should have 0 pending request", AINaAccept.getPendingRequests().size(), 0);
        Assert.assertEquals("User Jens should have 1 friend", jensNaAccept.getFriends().size(), 2);
        Assert.assertEquals("User AIAccount should have 1 friend", AINaAccept.getFriends().size(), 2);
    }


    @Test
    public void testDeclineRequest() {
        User stino = getUserByUsernameService.getUserByUsername("Stino");
        User AI = getUserByUsernameService.getUserByUsername("AIAccount");

        sendFriendRequestService.sendFriendRequest(stino, AI);

        String reply = friendRequestManager.declineFriendRequest(stino.getUsername(), AI.getUsername());
        Assert.assertEquals("Declined friend request!", reply);

        User stinoNaDecline = getUserByUsernameService.getUserByUsername("Jens");
        User AINaDecline = getUserByUsernameService.getUserByUsername("AIAccount");

        Assert.assertEquals("User Jens should have 0 sent requests", stinoNaDecline.getSentRequests().size(), 0);
        Assert.assertEquals("User AIAccount should have 0 pending request", AINaDecline.getPendingRequests().size(), 0);
    }
}

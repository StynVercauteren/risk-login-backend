package be.kdg.riskymerge.login.Integration;

import antlr.TokenStreamException;
import be.kdg.riskymerge.managers.AuthManager;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.TokenRepository;
import be.kdg.riskymerge.services.Auth.ConfirmPasswordTokenService;
import be.kdg.riskymerge.services.Auth.UpdatePasswordService;
import be.kdg.riskymerge.services.GetUser.GetUserByEmailService;
import be.kdg.riskymerge.services.SendMailService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.util.ReflectionTestUtils;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ForgotPasswordTest {
    @Autowired
    public AuthManager authManager;

    @Mock
    private SendMailService sendMailService;

    @Autowired
    private ConfirmPasswordTokenService confirmPasswordTokenService;

    @Autowired
    private UpdatePasswordService updatePasswordService;

    @Autowired
    private GetUserByEmailService getUserByEmailService;

    @Autowired
    private TokenRepository tokenRepository;

    @BeforeAll
    void setup() {
        ReflectionTestUtils.setField(authManager, "sendMailService" , sendMailService);
    }

    @Test
    public void testUserForgotPassword() throws TokenStreamException {
        String testmail = "styn.vercauteren@student.kdg.be";
        String response = authManager.resetPassword(testmail);
        Assert.assertEquals(response, "Succesfully sent mail!");

        User user = getUserByEmailService.getUserByEmail(testmail);
        Token verificationToken = tokenRepository.findByUser(user);
        Boolean result = confirmPasswordTokenService.validatePasswordResetToken(verificationToken.getUser().getUser_id(), verificationToken.getToken());
        Assert.assertTrue(result);

        String update = updatePasswordService.updatePassword("newPassword", user);
        Assert.assertEquals("Succesfully updated password!", update);
    }
}

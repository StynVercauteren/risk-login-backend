package be.kdg.riskymerge.controllers;

import be.kdg.riskymerge.managers.FriendRequestManager;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/friendRequest")
public class FriendRequestController {
    private final FriendRequestManager friendRequestManager;

    @Autowired
    public FriendRequestController(FriendRequestManager friendRequestManager) {
        this.friendRequestManager = friendRequestManager;
    }

    @PostMapping("/sendRequest")
    public ResponseEntity<?> sendFriendRequest(@RequestParam("currentUserName") String currentUserName, @RequestParam("otherUserName") String otherUserName) {
        try {
            String response = friendRequestManager.sendFriendRequest(currentUserName, otherUserName);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/acceptRequest")
    public ResponseEntity<?> acceptFriendRequest(@RequestParam String username, @RequestParam String friendUsername) {
        try {
            String response = friendRequestManager.acceptFriendRequest(username, friendUsername);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/declineRequest")
    public ResponseEntity<?> declineFriendRequest(@RequestParam String username, @RequestParam String friendUsername) {
        try {
            String response = friendRequestManager.declineFriendRequest(username, friendUsername);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }
}

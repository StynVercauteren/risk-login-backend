package be.kdg.riskymerge.controllers;

import be.kdg.riskymerge.managers.UpdateAccountManager;
import be.kdg.riskymerge.models.DTO.UpdateAccountDTO;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/updateAccount")
public class UpdateUserController {
    private final UpdateAccountManager updateAccountManager;

    @Autowired
    public UpdateUserController(UpdateAccountManager updateAccountManager) {
        this.updateAccountManager = updateAccountManager;
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateAccount(@RequestBody UpdateAccountDTO user) {
        try {
            String response = updateAccountManager.update(user);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }
}

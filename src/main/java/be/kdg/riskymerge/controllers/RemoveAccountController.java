package be.kdg.riskymerge.controllers;

import be.kdg.riskymerge.managers.RemoveAccountManager;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/removeAccount")
public class RemoveAccountController {
    private final RemoveAccountManager removeAccountManager;

    @Autowired
    public RemoveAccountController(RemoveAccountManager removeAccountManager) {
        this.removeAccountManager = removeAccountManager;
    }

    @PostMapping("/remove")
    public ResponseEntity<?> removeUser(@Valid @RequestBody JwtResponse user) {
        try {
            String response = removeAccountManager.removeUser(user);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }
}

package be.kdg.riskymerge.controllers;

import be.kdg.riskymerge.managers.ProfilePictureManager;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@RestController
@RequestMapping("/api/profilepicture")
public class ProfilePictureController {
    private final ProfilePictureManager profilePictureManager;

    public ProfilePictureController(ProfilePictureManager profilePictureManager) {
        this.profilePictureManager = profilePictureManager;
    }

    @PostMapping("/upload")
    public ResponseEntity<?> handleFileUpload(@RequestPart(value = "file") final MultipartFile uploadfile, String username, String accessToken) {
        try {
            profilePictureManager.saveImage(uploadfile, username, accessToken);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
        return ResponseEntity.ok("Succesfully uploaded profile picture!");
    }

    @GetMapping("/image")
    public Resource loadImage(String username) {
        return profilePictureManager.getProfilePicture(username);
    }
}

package be.kdg.riskymerge.controllers;

import be.kdg.riskymerge.managers.FriendsManager;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/friends")
public class FriendsController {
    private final FriendsManager friendsManager;

    @Autowired
    public FriendsController(FriendsManager friendsManager) {
        this.friendsManager = friendsManager;
    }

    @GetMapping("/checkIfUserIsFriend")
    public ResponseEntity<?> checkIfUserIsFriend(@RequestParam("currentUserName") String currentUserName, @RequestParam("otherUserName") String otherUserName) {
        try {
            boolean response = friendsManager.checkIfUserIsFriend(currentUserName, otherUserName);
            return ResponseEntity.ok(response);
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/checkIfUserHasPendingRequest")
    public ResponseEntity<?> checkIfUserHasPendingRequest(@RequestParam("currentUserName") String currentUserName, @RequestParam("otherUserName") String otherUserName) {
        try {
            boolean response = friendsManager.checkIfUserHasPendingRequest(currentUserName, otherUserName);
            return ResponseEntity.ok(response);
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/getPendingRequests")
    public ResponseEntity<?> getPendingRequests(@RequestParam("username") String username) {
        try {
            List<UserDetails> pendingRequests = friendsManager.getPendingRequests(username);
            return ResponseEntity.ok(pendingRequests);
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/deleteFriend")
    public ResponseEntity<?> deleteFriend(@RequestParam String username, @RequestParam String friendUsername) {
        try {
            String response = friendsManager.deleteFriend(username, friendUsername);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/getFriends")
    public ResponseEntity<?> getFriends(@RequestParam("username") String username) {
        try {
            List<UserDetails> pendingRequests = friendsManager.getFriends(username);
            return ResponseEntity.ok(pendingRequests);
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/getSentRequests")
    public ResponseEntity<?> getSentRequests(@RequestParam("username") String username) {
        try {
            List<UserDetails> sentRequests = friendsManager.getSentRequests(username);
            return ResponseEntity.ok(sentRequests);
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }
}

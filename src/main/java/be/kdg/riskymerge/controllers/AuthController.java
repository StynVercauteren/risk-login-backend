package be.kdg.riskymerge.controllers;

import javax.naming.AuthenticationException;

import javax.validation.Valid;

import antlr.TokenStreamException;
import be.kdg.riskymerge.managers.AuthManager;
import be.kdg.riskymerge.models.DTO.*;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.models.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthManager authManager;

    @Autowired
    public AuthController(AuthManager authManager) {
        this.authManager = authManager;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {
        try {
            JwtResponse response = authManager.signin(loginDTO);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupDTO signupDTO) {
        try {
            String response = authManager.register(signupDTO);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/verifyuser")
    public ResponseEntity<?> verifyUser(@Valid @RequestBody UserDTO user) {
        try {
            Boolean response = authManager.verifyUser(user);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/confirmEmail")
    public ResponseEntity<?> confirmEmail(@RequestParam("token") String token) {
        try {
            String response = authManager.confirmEmail(token);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        } catch (TokenStreamException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestParam("email") String email) {
        try {
            String response = authManager.resetPassword(email);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/confirmPassword")
    public ResponseEntity<?> confirmPassword(@RequestParam("id") int id, @RequestParam("token") String token) {
        try {
            boolean response = authManager.validatePasswordResetToken(id, token);
            return ResponseEntity.ok(response);
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        } catch (TokenStreamException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestParam("id") int id, @RequestParam("password") String password) {
        try {
            String response = authManager.updatePassword(id, password);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(response));
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }

    @GetMapping("/getUserData")
    public ResponseEntity<?> getUserData(@RequestParam("username") String username) {
        try {
            UserDetails response = authManager.GetUserDetails(username);
            return ResponseEntity.ok(response);
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(e.getMessage()));
        }
    }
}
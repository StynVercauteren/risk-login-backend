package be.kdg.riskymerge.models.DTO;

public class NotificationDto {

    private String content;
    private String title;

    public NotificationDto() {
    }

    public NotificationDto(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

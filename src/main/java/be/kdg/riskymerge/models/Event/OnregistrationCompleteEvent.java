package be.kdg.riskymerge.models.Event;

import be.kdg.riskymerge.models.User;
import org.springframework.context.ApplicationEvent;

public class OnregistrationCompleteEvent extends ApplicationEvent {
    private User user;

    public OnregistrationCompleteEvent(User user) {
        super(user);

        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

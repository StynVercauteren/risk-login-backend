package be.kdg.riskymerge.managers;

import be.kdg.riskymerge.models.DTO.NotificationDto;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.FriendRequest.AcceptFriendRequestService;
import be.kdg.riskymerge.services.FriendRequest.RemoveRequestsService;
import be.kdg.riskymerge.services.FriendRequest.SendFriendRequestService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.SendUserNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * This manager is responsible for everything concerning friend requests.
 * A user can send someone else a request if they're not friends yet, or accept / decline a request that has been sent before.
 */
@Service
public class FriendRequestManager {
    private static final Logger logger = LoggerFactory.getLogger(FriendRequestManager.class);

    private final AcceptFriendRequestService acceptFriendRequestService;
    private final GetUserByUsernameService getUserByUsernameService;
    private final RemoveRequestsService removeRequestsService;
    private final SendUserNotificationService sendUserNotificationService;
    private final SendFriendRequestService sendFriendRequestService;

    public FriendRequestManager(AcceptFriendRequestService acceptFriendRequestService, GetUserByUsernameService getUserByUsernameService, RemoveRequestsService removeRequestsService, SendUserNotificationService sendUserNotificationService, SendFriendRequestService sendFriendRequestService) {
        this.acceptFriendRequestService = acceptFriendRequestService;
        this.getUserByUsernameService = getUserByUsernameService;
        this.removeRequestsService = removeRequestsService;
        this.sendUserNotificationService = sendUserNotificationService;
        this.sendFriendRequestService = sendFriendRequestService;
    }

    public String acceptFriendRequest(String username, String friendUsername) {
        User user = getUserByUsernameService.getUserByUsername(username);

        User friend = getUserByUsernameService.getUserByUsername(friendUsername);

        removeRequestsService.removeRequests(user, friend);

        String result = acceptFriendRequestService.acceptFriendRequest(user, friend);

        NotificationDto notificationDto = new NotificationDto("Request succeeded", "You are now friends with " + friend.getUsername());
        sendUserNotificationService.sendUserNotification(user.getUsername(), notificationDto);

        return result;
    }

    public String declineFriendRequest(String username, String friendUsername) {
        User user = getUserByUsernameService.getUserByUsername(username);

        User friend = getUserByUsernameService.getUserByUsername(friendUsername);

        removeRequestsService.removeRequests(user, friend);

        NotificationDto notificationDto = new NotificationDto("Request declined", friend.getUsername() + " has declined your request.");
        sendUserNotificationService.sendUserNotification(user.getUsername(), notificationDto);

        logger.info("Declined friend request!");
        return "Declined friend request!";
    }

    public String sendFriendRequest(String username, String friendusername) {
        User user = getUserByUsernameService.getUserByUsername(username);

        User friend = getUserByUsernameService.getUserByUsername(friendusername);

        String response = sendFriendRequestService.sendFriendRequest(user, friend);

        NotificationDto notificationDto = new NotificationDto("Request sent!", "Your request has been sent to " + friendusername);
        sendUserNotificationService.sendUserNotificationWithButtons(username, notificationDto);

        return response;
    }

}

package be.kdg.riskymerge.managers;

import be.kdg.riskymerge.models.DTO.NotificationDto;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.Friends.CheckIfUserHasPendingRequestService;
import be.kdg.riskymerge.services.Friends.CheckIfUserIsFriendService;
import be.kdg.riskymerge.services.Friends.DeleteFriendService;
import be.kdg.riskymerge.services.Friends.GetUserSetService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.SendUserNotificationService;
import be.kdg.riskymerge.services.UserDetails.UserDetailsServiceImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This manager is responsible for everything concerning friends.
 * The webclient uses this to check if a certain user is friends with someone or if someone has pending requests so
 * that it can check that without need an unnecessary list of friends.
 * A user can also get his details or delete his friendship with someone else.
 */
@Service
public class FriendsManager {
    private final CheckIfUserHasPendingRequestService checkIfUserHasPendingRequestService;
    private final CheckIfUserIsFriendService checkIfUserIsFriendService;
    private final GetUserByUsernameService getUserByUsernameService;
    private final DeleteFriendService deleteFriendService;
    private final SendUserNotificationService sendUserNotificationService;
    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final GetUserSetService getUserSetService;

    public FriendsManager(CheckIfUserHasPendingRequestService checkIfUserHasPendingRequestService, CheckIfUserIsFriendService checkIfUserIsFriendService, GetUserByUsernameService getUserByUsernameService, DeleteFriendService deleteFriendService, SendUserNotificationService sendUserNotificationService, UserDetailsServiceImpl userDetailsServiceImpl, GetUserSetService getUserSetService) {
        this.checkIfUserHasPendingRequestService = checkIfUserHasPendingRequestService;
        this.checkIfUserIsFriendService = checkIfUserIsFriendService;
        this.getUserByUsernameService = getUserByUsernameService;
        this.deleteFriendService = deleteFriendService;
        this.sendUserNotificationService = sendUserNotificationService;
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.getUserSetService = getUserSetService;
    }

    public Boolean checkIfUserHasPendingRequest(String currentUserName, String otherUserName) {
        User friend = getUserByUsernameService.getUserByUsername(otherUserName);
        return checkIfUserHasPendingRequestService.checkIfUserHasPendingRequest(currentUserName, friend);
    }

    public Boolean checkIfUserIsFriend(String currentUserName, String otherUserName) {
        User currentUser = getUserByUsernameService.getUserByUsername(currentUserName);
        return checkIfUserIsFriendService.CheckIfUserIsFriend(currentUser, otherUserName);
    }

    public String deleteFriend(String username, String friendUsername) {
        User user = getUserByUsernameService.getUserByUsername(username);

        User friend = getUserByUsernameService.getUserByUsername(friendUsername);

        String result = deleteFriendService.deleteFriend(user, friend);
        NotificationDto notificationDto = new NotificationDto("Friend deleted!", "You have deleted " + friend.getUsername() + " as a friend.");
        sendUserNotificationService.sendUserNotification(user.getUsername(), notificationDto);

        notificationDto = new NotificationDto("Friend deleted!",  user.getUsername() + " has deleted you as a friend.");
        sendUserNotificationService.sendUserNotification(friend.getUsername(), notificationDto);

        return result;
    }

    public List<UserDetails> getFriends(String username) {
        User user = getUserByUsernameService.getUserByUsername(username);
        Set<User> users = getUserSetService.getSet(user, "friend");

        return getList(users);
    }

    public List<UserDetails> getPendingRequests(String username) {
        User user = getUserByUsernameService.getUserByUsername(username);
        Set<User> users = getUserSetService.getSet(user, "pending");

        return getList(users);
    }

    public List<UserDetails> getSentRequests(String username) {
        User user = getUserByUsernameService.getUserByUsername(username);

        Set<User> users = getUserSetService.getSet(user, "sent");

        return getList(users);
    }

    public List<UserDetails> getList(Set<User> users) {
        List<UserDetails> usersToReturn = new ArrayList<>();

        for (User user1 : users) {
            usersToReturn.add(userDetailsServiceImpl.loadUserByUsername(user1.getUsername()));
        }

        return usersToReturn;
    }
}

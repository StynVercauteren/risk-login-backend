package be.kdg.riskymerge.managers;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.ProfilePicture.GetProfilePictureService;
import be.kdg.riskymerge.services.ProfilePicture.ImageUploaderService;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * This manager handles the upload and retrieval of a profile picture.
 */
@Service
public class ProfilePictureManager {
    private final GetProfilePictureService getProfilePictureService;
    private final GetUserByUsernameService getUserByUsernameService;
    private final ImageUploaderService imageUploaderService;

    public ProfilePictureManager(GetProfilePictureService getProfilePictureService, GetUserByUsernameService getUserByUsernameService, ImageUploaderService imageUploaderService) {
        this.getProfilePictureService = getProfilePictureService;
        this.getUserByUsernameService = getUserByUsernameService;
        this.imageUploaderService = imageUploaderService;
    }

    public Resource getProfilePicture(String username) {
        User user = getUserByUsernameService.getUserByUsername(username);

        return getProfilePictureService.getProfilePicture(user);
    }

    public void saveImage(MultipartFile uploadfile, String username, String accessToken) throws IOException {
        User user = getUserByUsernameService.getUserByUsername(username);

        imageUploaderService.saveImage(uploadfile, user, accessToken);
    }
}

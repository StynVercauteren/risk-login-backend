package be.kdg.riskymerge.managers;

import be.kdg.riskymerge.models.DTO.NotificationDto;
import be.kdg.riskymerge.models.DTO.UpdateAccountDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.services.GetUser.GetUserByIdService;
import be.kdg.riskymerge.services.UpdateAccountService;
import be.kdg.riskymerge.services.SendUserNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UpdateAccountManager {
    private static final Logger logger = LoggerFactory.getLogger(UpdateAccountManager.class);

    private final UpdateAccountService updateAccountService;
    private final GetUserByIdService getUserByIdService;
    private final SendUserNotificationService sendUserNotificationService;

    public UpdateAccountManager(UpdateAccountService updateAccountService, GetUserByIdService getUserByIdService, SendUserNotificationService sendUserNotificationService) {
        this.updateAccountService = updateAccountService;
        this.getUserByIdService = getUserByIdService;
        this.sendUserNotificationService = sendUserNotificationService;
    }

    public String update(UpdateAccountDTO user) {
        User dbUser = getUserByIdService.getUserById(user.getId());

        String response = updateAccountService.updateAccount(user, dbUser);

        if (response.equals("username")) {
            NotificationDto notificationDto = new NotificationDto("Succes updating", "You have succesfully updated your username.");
            sendUserNotificationService.sendUserNotification(dbUser.getUsername(), notificationDto);
            logger.info("Succesfully updated username!");
            return "Succesfully updated user!";
        } else if (response.equals("email")) {
            NotificationDto notificationDto = new NotificationDto("Succes updating", "You have succesfully updated your email.");
            sendUserNotificationService.sendUserNotification(dbUser.getUsername(), notificationDto);
            logger.info("Succesfully updated user!");
            return "Succesfully updated email!";
        } else {
            NotificationDto notificationDto = new NotificationDto("Succes updating", "You have succesfully updated your password.");
            sendUserNotificationService.sendUserNotification(dbUser.getUsername(), notificationDto);
            logger.info("Succesfully updated user!");
            return "Succesfully updated password!";
        }
    }
}

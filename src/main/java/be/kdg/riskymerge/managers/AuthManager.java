package be.kdg.riskymerge.managers;

import antlr.TokenStreamException;
import be.kdg.riskymerge.models.DTO.LoginDTO;
import be.kdg.riskymerge.models.DTO.SignupDTO;
import be.kdg.riskymerge.models.DTO.UserDTO;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.services.Auth.*;
import be.kdg.riskymerge.services.Auth.ConfirmPasswordTokenService;
import be.kdg.riskymerge.services.CreateTokenService;
import be.kdg.riskymerge.services.GetUser.GetUserByEmailService;
import be.kdg.riskymerge.services.GetUser.GetUserByIdService;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.SendMailService;
import be.kdg.riskymerge.services.Token.TokenService;
import be.kdg.riskymerge.services.UserDetails.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.UUID;

/**
 *
 * This manager is responsable for haling everything concerning doing with authentication.
 * This is mainly the process of creating an account and logging in.
 * This also means other stuff like validating the reset token for a password-reset.
 */
@Service
public class AuthManager {
    private static final Logger logger = LoggerFactory.getLogger(AuthManager.class);

    private final ConfirmEmailService confirmEmailService;
    private final GetUserByUsernameService getUserByUsernameService;
    private final TokenService tokenService;
    private final UserDetailsServiceImpl userDetailsService;
    private final UserDataService userDataService;
    private final SendMailService sendMailService;
    private final CreateTokenService createTokenService;
    private final GetUserByEmailService getUserByEmailService;
    private final SigninService signinService;
    private final UpdatePasswordService updatePasswordService;
    private final GetUserByIdService getUserByIdService;
    private final VerifyUserService verifyUserService;
    private final SignupService signupService;
    private final ConfirmPasswordTokenService confirmPasswordTokenService;

    @Value("${notification-service.url}")
    String notificationServiceUrl;

    @Value("${frontend.url}")
    String frontendUrl;

    public AuthManager(ConfirmEmailService confirmEmailService, GetUserByUsernameService getUserByUsernameService, TokenService tokenService, UserDetailsServiceImpl userDetailsService, UserDataService userDataService, SendMailService sendMailService, CreateTokenService createTokenService, GetUserByEmailService getUserByEmailService, SigninService signinService, UpdatePasswordService updatePasswordService, GetUserByIdService getUserByIdService, VerifyUserService verifyUserService, SignupService signupService, ConfirmPasswordTokenService confirmPasswordTokenService) {
        this.confirmEmailService = confirmEmailService;
        this.getUserByUsernameService = getUserByUsernameService;
        this.tokenService = tokenService;
        this.userDetailsService = userDetailsService;
        this.userDataService = userDataService;
        this.sendMailService = sendMailService;
        this.createTokenService = createTokenService;
        this.getUserByEmailService = getUserByEmailService;
        this.signinService = signinService;
        this.updatePasswordService = updatePasswordService;
        this.getUserByIdService = getUserByIdService;
        this.verifyUserService = verifyUserService;
        this.signupService = signupService;
        this.confirmPasswordTokenService = confirmPasswordTokenService;
    }

    public String confirmEmail(String token) throws TokenStreamException {
        Token verificationToken = tokenService.getToken(token);

        User user = getUserByUsernameService.getUserByUsername(verificationToken.getUser().getUsername());

        return confirmEmailService.confirmEmail(user);
    }

    public UserDetails GetUserDetails(String username) {
        return userDataService.getUserData(username, userDetailsService);
    }

    public String resetPassword(String email) {
        String token = UUID.randomUUID().toString();
        User user = getUserByEmailService.getUserByEmail(email);
        createTokenService.createVerificationToken(user, token);
        logger.info("Created and saved token " + token + " for user " + user.getUsername());

        String subject = "Reset your password!";
        logger.info("Created subject for mail: " + subject);

        String content = "Please click the link so you can reset your password. " + frontendUrl + "/newPassword?id=" + user.getUser_id() + "&token=" + token;
        logger.info("Created content for mail: " + content);

        String mailSenderUrl = String.format("%s/api/sendEmail", notificationServiceUrl);

        sendMailService.sendMail(user, subject, content, mailSenderUrl);
        logger.info("Sent mail to " + user.getUsername());

        return "Succesfully sent mail!";
    }

    public JwtResponse signin(LoginDTO loginDTO) throws AuthenticationException {
        return signinService.signin(loginDTO, getUserByUsernameService.getUserByUsername(loginDTO.getUsername()));
    }

    public String updatePassword(int id, String password) {
        return updatePasswordService.updatePassword(password, getUserByIdService.getUserById(id));
    }

    public Boolean verifyUser(UserDTO userDTO) throws AuthenticationException {
        return verifyUserService.verifyUser(userDTO, getUserByUsernameService.getUserByUsername(userDTO.getUsername()));
    }

    public String register(SignupDTO signUpRequest) throws AuthenticationException {
        return signupService.register(signUpRequest);
    }

    public boolean validatePasswordResetToken(long id, String token) throws TokenStreamException {
        return confirmPasswordTokenService.validatePasswordResetToken(id, token);
    }
}

package be.kdg.riskymerge.managers;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.services.GetUser.GetUserByUsernameService;
import be.kdg.riskymerge.services.RemoveAccountService;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;

@Service
public class RemoveAccountManager {
    private final RemoveAccountService removeAccountService;
    private final GetUserByUsernameService getUserByUsernameService;

    public RemoveAccountManager(RemoveAccountService removeAccountService, GetUserByUsernameService getUserByUsernameService) {
        this.removeAccountService = removeAccountService;
        this.getUserByUsernameService = getUserByUsernameService;
    }

    public String removeUser(JwtResponse user) throws AuthenticationException {
        User dbUser = getUserByUsernameService.getUserByUsername(user.getUsername());

        return removeAccountService.remove(user, dbUser);
    }
}

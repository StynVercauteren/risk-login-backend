package be.kdg.riskymerge.services;

import be.kdg.riskymerge.events.IUserService;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.repositories.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateTokenService implements IUserService {
    private static final Logger logger = LoggerFactory.getLogger(CreateTokenService.class);

    private final TokenRepository tokenRepository;

    @Autowired
    public CreateTokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public void createVerificationToken(User user, String token) {
        Token myToken = new Token(token, user);
        tokenRepository.save(myToken);
        logger.info("Saved registration-token with value " + token + " for user " + user.getUsername());
    }
}

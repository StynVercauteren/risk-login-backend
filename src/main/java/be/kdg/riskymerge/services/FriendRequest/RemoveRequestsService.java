package be.kdg.riskymerge.services.FriendRequest;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RemoveRequestsService {
    private static final Logger logger = LoggerFactory.getLogger(RemoveRequestsService.class);

    private final UserRepository userRepository;

    public RemoveRequestsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void removeRequests(User user, User friend) {
        user.getSentRequests().removeIf(u -> u.getUser_id() == friend.getUser_id());
        logger.info("Removed sent request for " + user.getUsername());
        friend.getPendingRequests().removeIf(u -> u.getUser_id() == user.getUser_id());
        logger.info("Removed pending request for " + friend.getUsername());

        userRepository.save(user);
        logger.info("Updated " + user.getUsername() + " in the database");

        userRepository.save(friend);
        logger.info("Updated " + friend.getUsername() + " in the database");
    }
}

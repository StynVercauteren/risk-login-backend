package be.kdg.riskymerge.services.FriendRequest;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AcceptFriendRequestService {
    private static final Logger logger = LoggerFactory.getLogger(AcceptFriendRequestService.class);

    private final UserRepository userRepository;

    public AcceptFriendRequestService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String acceptFriendRequest(User user, User friend) {
        user.getFriends().add(friend);
        logger.info("Added friend " + friend.getUsername() + " for user " + user.getUsername());
        friend.getFriends().add(user);
        logger.info("Added friend " + user.getUsername() + " for user " + friend.getUsername());

        userRepository.save(user);
        logger.info("Updated " + user.getUsername() + " in the database");

        userRepository.save(friend);
        logger.info("Updated " + friend.getUsername() + " in the database");

        logger.info("Befriended both users succesfully!");
        return "You are now friends with " + friend.getUsername();
    }
}

package be.kdg.riskymerge.services.FriendRequest;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SendFriendRequestService {
    private static final Logger logger = LoggerFactory.getLogger(SendFriendRequestService.class);

    private final UserRepository userRepository;

    public SendFriendRequestService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String sendFriendRequest(User user, User friend) {
        user.getSentRequests().add(friend);
        logger.info("Added sent request to user " + user.getUsername());

        userRepository.save(user);
        logger.info("Updated user " + user.getUsername() + " in database");

        friend.getPendingRequests().add(user);
        logger.info("Added pending request to user " + friend.getUsername());

        userRepository.save(friend);
        logger.info("Updated user " + friend.getUsername() + " in database");

        logger.info("Sent pending friend request!");
        return "Sent pending friend request!";
    }
}

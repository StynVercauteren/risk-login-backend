package be.kdg.riskymerge.services.Friends;

import be.kdg.riskymerge.models.User;
import org.springframework.stereotype.Service;

@Service
public class CheckIfUserIsFriendService {
    public Boolean CheckIfUserIsFriend(User currentUser, String otherUser) {
        if (currentUser.getUsername().equals(otherUser)) {
            return false;
        }

        for (User friend : currentUser.getFriends()) {
            if (friend.getUsername().equals(otherUser)) {
                return true;
            }
        }

        return false;
    }
}

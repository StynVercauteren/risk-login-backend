package be.kdg.riskymerge.services.Friends;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DeleteFriendService {
    private static final Logger logger = LoggerFactory.getLogger(DeleteFriendService.class);
    private final UserRepository userRepository;

    public DeleteFriendService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String deleteFriend(User user, User friend) {
        user.getFriends().removeIf(u -> u.getUser_id() == friend.getUser_id());
        logger.info("Removed friend " + friend.getUsername() +  " from  " + user.getUsername());
        friend.getFriends().removeIf(u -> u.getUser_id() == user.getUser_id());
        logger.info("Removed friend " + user.getUsername() +  " from  " + friend.getUsername());

        userRepository.save(user);
        logger.info("Updated " + user.getUsername() + " in the database");

        userRepository.save(friend);
        logger.info("Updated " + friend.getUsername() + " in the database");

        logger.info("Removed friend!");
        return "Removed friend!";
    }
}

package be.kdg.riskymerge.services.Friends;

import be.kdg.riskymerge.models.User;
import org.springframework.stereotype.Service;

@Service
public class CheckIfUserHasPendingRequestService {
    public Boolean checkIfUserHasPendingRequest(String currentUserName, User friend) {
        for (User pendingRequests : friend.getPendingRequests()) {
            if (pendingRequests.getUsername().equals(currentUserName)) {
                return true;
            }
        }

        return false;
    }
}

package be.kdg.riskymerge.services.Friends;

import be.kdg.riskymerge.models.User;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * This services returns a user's set based on the needed method.
 */
@Service
public class GetUserSetService {

    public Set<User> getSet(User user, String method) {
        if (method.equals("friend")) {
            return user.getFriends();
        } else if (method.equals("pending")){
            return user.getPendingRequests();
        } else if (method.equals("sent")) {
            return user.getSentRequests();
        }

        return new HashSet<>();
    }
}

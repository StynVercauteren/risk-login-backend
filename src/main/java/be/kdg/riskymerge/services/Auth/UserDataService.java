package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.services.UserDetails.UserDetailsServiceImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * This service is used to return the data of a certain user. It's uses on the webclient to look at another persons
 * profile.
 */
@Service
public class UserDataService {
    public UserDetails getUserData(String username, UserDetailsServiceImpl userDetailsService) {
        return userDetailsService.loadUserByUsername(username);
    }
}

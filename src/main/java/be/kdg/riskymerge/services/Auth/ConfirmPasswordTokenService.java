package be.kdg.riskymerge.services.Auth;

import antlr.TokenStreamException;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.repositories.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * When a user has forgotten his password and wants to reset it, he'll get a token so the server knows it's him.
 * This service will be called after a user has confirmed his ownership trough email.
 * The service returns true if the token is valid & the user from the database matches his id.
 */
@Service
public class ConfirmPasswordTokenService {
    private static final Logger logger = LoggerFactory.getLogger(ConfirmPasswordTokenService.class);

    private final TokenRepository tokenRepository;

    public ConfirmPasswordTokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public boolean validatePasswordResetToken(long id, String token) throws TokenStreamException {
        Token passToken = tokenRepository.findByToken(token);
        logger.info("Retrieved token " + token + " from database.");

        if (passToken == null) {
            logger.error("Invalid token!");
            throw new TokenStreamException("Invalid token!");
        }

        if ((passToken.getUser().getUser_id() != id)) {
            logger.error("Could not find user!");
            throw new UsernameNotFoundException("Could not find user!");
        }

        logger.info("Validated password-reset-token.");
        return true;
    }
}

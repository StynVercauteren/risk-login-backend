package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.models.Event.OnregistrationCompleteEvent;
import be.kdg.riskymerge.models.ERole;
import be.kdg.riskymerge.models.Role;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.DTO.SignupDTO;
import be.kdg.riskymerge.repositories.RoleRepository;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.HashSet;
import java.util.Set;

@Service
public class SignupService {
    private static final Logger logger = LoggerFactory.getLogger(SignupService.class);

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public SignupService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, ApplicationEventPublisher eventPublisher) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.eventPublisher = eventPublisher;
    }

    public String register(SignupDTO signUpRequest) throws AuthenticationException {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            logger.error("Error: Username " + signUpRequest.getUsername() +" is already taken!");
            throw new AuthenticationException("Error: Username " + signUpRequest.getUsername() +" is already taken!");
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            logger.error("Error: Username " + signUpRequest.getUsername() +" is already taken!");
            throw new AuthenticationException("Error: Email " + signUpRequest.getEmail() +" is already taken!");
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail().toLowerCase(),
                encoder.encode(signUpRequest.getPassword()));
        logger.info("Created user-account with username: " + signUpRequest.getUsername());

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findFirstByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findFirstByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findFirstByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findFirstByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        logger.info("Set roles for user with username " + user.getUsername());

        user.setEnabled(false);
        logger.info("Set enabled false for user with username " + user.getUsername());

        userRepository.save(user);
        logger.info("Saved user with " + user.getUsername() + " in the database.");

        try {
            //Gebruik gemaakt van ApplicationEvent om token te maken & sturen om het "collateral" back-end tasks zijn
            eventPublisher.publishEvent(new OnregistrationCompleteEvent(user));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }

        logger.info("User registered successfully!");
        return "User registered successfully!";
    }
}

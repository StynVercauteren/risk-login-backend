package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.jwt.JwtUtils;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.DTO.LoginDTO;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.repositories.UserRepository;
import be.kdg.riskymerge.services.UserDetails.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This service is responsable for logging in a user. It also gives the user a default-profilepicture and not allow
 * the user to log in if he hasn't verified his / her mail yet.
 */
@Service
public class SigninService {
    private static final Logger logger = LoggerFactory.getLogger(SigninService.class);

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    private final UserRepository userRepository;

    @Autowired
    public SigninService(AuthenticationManager authenticationManager, JwtUtils jwtUtils, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.userRepository = userRepository;
    }

    public JwtResponse signin(LoginDTO loginDTO, User checkUser) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        logger.info("Authenticated user with username: " + loginDTO.getUsername());

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        userRepository.setUserInfoById(jwt, userDetails.getId());
        logger.info("Generated and saved JWT-token: " + jwt);

        String defaultProfilePic;
        if (checkUser.getProfilepicture() == null) {
            defaultProfilePic = "default.png";
        } else {
            defaultProfilePic = checkUser.getProfilepicture();
        }

        if (!checkUser.isEnabled()) {
            logger.error("User " + checkUser.getUsername() + " has not verified his mail yet!");
            throw new AuthenticationException("User has not verified mail yet!");
        }

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles,
                defaultProfilePic
                );
    }
}
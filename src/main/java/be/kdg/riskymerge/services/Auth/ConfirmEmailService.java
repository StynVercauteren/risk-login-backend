package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * After a user has registered his account, he will be forced to confirm his / her email. After confirming, this class
 * will be used to enable the user's account.
 */
@Service
public class ConfirmEmailService {
    private static final Logger logger = LoggerFactory.getLogger(ConfirmEmailService.class);

    private final UserRepository userRepository;

    public ConfirmEmailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String confirmEmail(User user) {
        userRepository.setEnabledById(true, user.getUser_id());
        logger.info("Set enabled true for user with username " + user.getUsername());

        logger.info("User verified succesfully!");
        return "Thank you for confirming your email!";
    }
}

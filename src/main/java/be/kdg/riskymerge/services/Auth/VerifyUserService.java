package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.DTO.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;

/**
 * When the game-backend needs verification that a user is logged in, this service will be used.
 * It receives the users current JWT-token and user itself and verifies it.
 */
@Service
public class VerifyUserService {
    private static final Logger logger = LoggerFactory.getLogger(VerifyUserService.class);

    public Boolean verifyUser(UserDTO userDTO, User user) throws AuthenticationException {
        if (!user.getToken().equals(userDTO.getToken())) {
            logger.error("Unvalid JWt-token!");
            throw new AuthenticationException("Unvalid JWt-token!");
        }

        logger.info("Returned verification for user with id: " + user.getUser_id() + " and username: " + user.getUsername());
        return true;
    }
}

package be.kdg.riskymerge.services.Auth;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UpdatePasswordService {
    private static final Logger logger = LoggerFactory.getLogger(UpdatePasswordService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UpdatePasswordService(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public String updatePassword(String password, User user) {
        user.setPassword(encoder.encode(password));
        userRepository.save(user);
        logger.info("Updated password for user " + user.getUsername());

        return "Succesfully updated password!";
    }
}

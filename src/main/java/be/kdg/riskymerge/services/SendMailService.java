package be.kdg.riskymerge.services;

import be.kdg.riskymerge.models.User;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * This service accesses the notification-service so it can send a mail.
 */
@Service
public class SendMailService {
    private static final Logger logger = LoggerFactory.getLogger(SendMailService.class);

    private final RestTemplate restTemplate;

    public SendMailService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void sendMail(User user, String subject, String content, String mailSenderUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        logger.info("Set content type to JSON for post to notification-service");

        JSONObject sendEmailDto = new JSONObject();
        sendEmailDto.put("receiverEmail", user.getEmail());
        sendEmailDto.put("subject", subject);
        sendEmailDto.put("content", content);
        logger.info("Created body to post to notification-service");

        HttpEntity<String> request = new HttpEntity<String>(sendEmailDto.toString(), headers);
        logger.info("Created request");

        restTemplate.postForObject(mailSenderUrl, request, String.class);
        logger.info("Sent mail to " + user.getEmail());
    }
}

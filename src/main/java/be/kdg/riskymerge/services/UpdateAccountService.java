package be.kdg.riskymerge.services;

import be.kdg.riskymerge.models.DTO.UpdateAccountDTO;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UpdateAccountService {
    private UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Autowired
    public UpdateAccountService(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public String updateAccount(UpdateAccountDTO user, User dbUser) {
        if (!dbUser.getUsername().equals(user.getUsername()) && user.getUsername() != null) {
            userRepository.setUserNameById(user.getUsername(), user.getId());
            return "username";
        }

        if (!dbUser.getEmail().equals(user.getEmail()) && user.getEmail() != null) {
            userRepository.setUserEmailById(user.getEmail(), user.getId());
            return "email";
        }

        if (user.getPassword() != null) {
            userRepository.setUserPasswordById(encoder.encode(user.getPassword()), user.getId());
            return "password";
        }

        return "done";
    }
}

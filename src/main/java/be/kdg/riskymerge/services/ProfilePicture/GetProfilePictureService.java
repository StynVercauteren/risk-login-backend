package be.kdg.riskymerge.services.ProfilePicture;

import be.kdg.riskymerge.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * This service is used to get a user's profile picture. If the picture doesn't exist in the database, it gets
 * accessed from the directory images.
 * This is needed because after saving an image, the JVM can't access new images in the directory.
 */
@Service
public class GetProfilePictureService {
    private static final Logger logger = LoggerFactory.getLogger(GetProfilePictureService.class);

    public Resource getProfilePicture(User user) {
        if (user.getFoto() == null) {
            logger.info("Returned profilepicture from directory images");
            return new ClassPathResource("images/" + user.getProfilepicture());
        }

        logger.info("Returned profilepicture from database");
        return new ByteArrayResource(user.getFoto());
    }
}

package be.kdg.riskymerge.services.ProfilePicture;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageUploaderService {
    private static final Logger logger = LoggerFactory.getLogger(ImageUploaderService.class);

    private UserRepository userRepository;

    @Autowired
    public ImageUploaderService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveImage(MultipartFile uploadfile, User user, String accessToken) throws IOException {
        if (!user.getToken().equals(accessToken)) {
            throw new AuthenticationException("Invalid JWT-token!");
        }

        userRepository.setUserProfilePictureById(uploadfile.getOriginalFilename(), user.getUser_id());
        logger.info("Set profile picture " + uploadfile.getOriginalFilename() + " for user " + user.getUsername());

        final byte[] bytes = uploadfile.getBytes();
        final Path path = Paths.get("src/main/resources/images/" + uploadfile.getOriginalFilename()).toAbsolutePath();
        Files.write(path, bytes);
        logger.info("Saved picture at path: " + path);

        userRepository.setUserDataById(bytes, user.getUser_id());
        logger.info("Saved picture in database for user : " + user.getUsername());
    }
}

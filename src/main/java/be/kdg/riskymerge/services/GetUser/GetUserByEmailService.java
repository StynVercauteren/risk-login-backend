package be.kdg.riskymerge.services.GetUser;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * This service get used by the manager to access a user's account by mail.
 * This reduces the need of doing so himself all the time.
 */
@Service
public class GetUserByEmailService {
    private static final Logger logger = LoggerFactory.getLogger(GetUserByEmailService.class);

    private final UserRepository userRepository;

    public GetUserByEmailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            logger.error("Could not find user " + user);
            throw new UsernameNotFoundException("Could not find user!");
        }
        return user;
    }
}

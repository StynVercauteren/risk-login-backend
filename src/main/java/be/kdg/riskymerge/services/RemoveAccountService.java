package be.kdg.riskymerge.services;

import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.response.JwtResponse;
import be.kdg.riskymerge.repositories.TokenRepository;
import be.kdg.riskymerge.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.naming.AuthenticationException;

import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class RemoveAccountService {
    private static final Logger logger = LoggerFactory.getLogger(RemoveAccountService.class);
    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;

    @Autowired
    public RemoveAccountService(UserRepository userRepository, TokenRepository tokenRepository) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
    }

    public String remove(JwtResponse user, User dbUser) throws AuthenticationException {
        Boolean bestaat = userRepository.existsByUsername(user.getUsername());
        if (!bestaat) {
            logger.error("Could not find user!");
            throw new NullPointerException("Could not find user!");
        }

        if (dbUser.getFriends().size() > 0) {
            for (Iterator<User> iterator = dbUser.getFriends().iterator(); iterator.hasNext();) {
                User friend = iterator.next();
                friend.getFriends().removeIf(originalUser -> originalUser.getUsername().equals(dbUser.getUsername()));
                userRepository.save(friend);
                iterator.remove();
            }
        }

        if (dbUser.getPendingRequests().size() > 0) {
            for (Iterator<User> iterator = dbUser.getPendingRequests().iterator(); iterator.hasNext();) {
                User pendingUser = iterator.next();
                pendingUser.getPendingRequests().removeIf(originalUser -> originalUser.getUsername().equals(dbUser.getUsername()));
                userRepository.save(pendingUser);
                iterator.remove();
            }
        }

        if (dbUser.getSentRequests().size() > 0) {
            for (Iterator<User> iterator = dbUser.getSentRequests().iterator(); iterator.hasNext();) {
                User sentUser = iterator.next();
                sentUser.getPendingRequests().removeIf(originalUser -> originalUser.getUsername().equals(dbUser.getUsername()));
                userRepository.save(sentUser);
                iterator.remove();
            }
        }

        userRepository.save(dbUser);

        List<Token> tokens = tokenRepository.findAll();
        for (Token token : tokens) {
            if (token.getUser().getUsername().equals(dbUser.getUsername())) {
                tokenRepository.delete(token);
            }
        }

        List<User> delete = userRepository.deleteByUsername(user.getUsername());
        if (delete.size() > 0) {
            logger.info("Deleted user: " + delete.get(0).getUsername());
            return "User deleted succesfully";
        }

        logger.error("Problem deleting user!");
        throw new AuthenticationException("Problem deleting user!");
    }
}

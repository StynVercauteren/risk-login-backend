package be.kdg.riskymerge.services;

import be.kdg.riskymerge.models.DTO.NotificationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * This service accesses the notification-service so it can send a notification.
 * This can be a normal one, or one with 'Yes' or 'No' buttons.
 */
@Service
public class SendUserNotificationService {
    private final RestTemplate restTemplate = new RestTemplate();
    private Logger logger = LoggerFactory.getLogger(SendUserNotificationService.class);

    @Value("${notification-service.url}")
    String notificationServiceUrl;

    public void sendUserNotificationWithButtons(String username, NotificationDto notificationDto) {
        String sendUserNotificationUrl = String.format("%s/api/friendrequest/user/%s", notificationServiceUrl, username);
        HttpEntity<NotificationDto> request = new HttpEntity<>(notificationDto);

        try {
            restTemplate.postForObject(sendUserNotificationUrl, request, String.class);
            logger.info("Sent notification with buttons to notification-service!");
        } catch (RestClientException e) {
            logger.error("Could not reach the notification service: " + e);
            throw new RestClientException("Could not reach the notification service");
        }
    }

    public void sendUserNotification(String username, NotificationDto notificationDto) {
        String sendUserNotificationUrl = String.format("%s/api/user/%s", notificationServiceUrl, username);
        HttpEntity<NotificationDto> request = new HttpEntity<>(notificationDto);

        try {
            restTemplate.postForObject(sendUserNotificationUrl, request, String.class);
            logger.info("Sent notification to notification-service!");
        } catch (RestClientException e) {
            logger.error("Could not reach the notification service: " + e);
            throw new RestClientException("Could not reach the notification service");
        }
    }
}

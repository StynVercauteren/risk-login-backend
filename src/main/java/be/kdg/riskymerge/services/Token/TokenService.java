package be.kdg.riskymerge.services.Token;

import antlr.TokenStreamException;
import be.kdg.riskymerge.models.Token;
import be.kdg.riskymerge.repositories.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TokenService {
    private static final Logger logger = LoggerFactory.getLogger(TokenService.class);

    private final TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public Token getToken(String token) throws TokenStreamException {
        Token token1 = tokenRepository.findByToken(token);

        if (token1 == null) {
            logger.error("Invalid token!");
            throw new TokenStreamException("Invalid token!");
        }

        return token1;
    }
}

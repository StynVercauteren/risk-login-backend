package be.kdg.riskymerge.events;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.Event.OnregistrationCompleteEvent;
import be.kdg.riskymerge.services.SendMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * This component uses the listener-pattern to send a mail to the user so he / he can confirm their mail.
 */

@Component
public class RegistrationListener implements ApplicationListener<OnregistrationCompleteEvent> {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationListener.class);

    private final SendMailService sendMailService;
    private final IUserService service;

    @Value("${notification-service.url}")
    String notificationServiceUrl;

    @Value("${frontend.url}")
    String frontendUrl;

    @Autowired
    public RegistrationListener(SendMailService sendMailService, IUserService service) {
        this.sendMailService = sendMailService;
        this.service = service;
    }

    @Override
    public void onApplicationEvent(OnregistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);
        logger.info("Used service to create verification token " + token + " for user " + user.getUsername());

        String subject = "Verify your email for Risk!";
        logger.info("Created subject for mail: " + subject);

        String content = "Hey! Welcome to the the Risk-platform. Please click the link so you can continue to use your profile. " + frontendUrl + "/login/" + token;
        logger.info("Created content for mail: " + content);

        String mailSenderUrl = String.format("%s/api/sendEmail", notificationServiceUrl);

        sendMailService.sendMail(user, subject, content, mailSenderUrl);
    }
}

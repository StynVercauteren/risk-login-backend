package be.kdg.riskymerge.events;

import be.kdg.riskymerge.models.User;

public interface IUserService {
    void createVerificationToken(User user, String token);
}

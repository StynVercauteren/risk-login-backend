package be.kdg.riskymerge.repositories;

import be.kdg.riskymerge.models.User;
import be.kdg.riskymerge.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token);

    Token findByUser(User user);
}

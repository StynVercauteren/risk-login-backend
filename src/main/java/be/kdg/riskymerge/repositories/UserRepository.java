package be.kdg.riskymerge.repositories;

import be.kdg.riskymerge.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    User findByEmail(String email);

    User findById(int id);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    @Transactional
    List<User> deleteByUsername(String username);

    @Transactional
    @Modifying
    @Query("update users u set u.token = ?1 where u.user_id = ?2")
    void setUserInfoById(String token, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.username = ?1 where u.user_id = ?2")
    void setUserNameById(String username, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.email = ?1 where u.user_id = ?2")
    void setUserEmailById(String email, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.profilepicture = ?1 where u.user_id = ?2")
    void setUserProfilePictureById(String profilepicture, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.password = ?1 where u.user_id = ?2")
    void setUserPasswordById(String password, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.enabled = ?1 where u.user_id = ?2")
    void setEnabledById(Boolean enabled, int id);

    @Transactional
    @Modifying
    @Query("update users u set u.foto = ?1 where u.user_id = ?2")
    void setUserDataById(byte[] data, int id);
}
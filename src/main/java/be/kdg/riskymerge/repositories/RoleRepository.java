package be.kdg.riskymerge.repositories;

import java.util.Optional;

import be.kdg.riskymerge.models.ERole;
import be.kdg.riskymerge.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findFirstByName(ERole name);
}